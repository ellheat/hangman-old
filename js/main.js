$(function() {
    var $window = $(window);
    var $nicknameInput = $('.nicknameInput');
    var $letterInput = $('.letterInput');
    var $usersList = $('#usersList')
    var $lettersList = $('#lettersList');
    var $placeholder = $('ul#placeholder');

    
    var $loginPage = $('.login.page');
    var $hangmanPage = $('.hangman.page');
    var $endPage = $('.end.page')
    
    
    var username;
    var letter;
    var points = 0;
    var mistake = 0;
    var $currentInput = $nicknameInput.focus();
    var word = 'przycisk';
    
    var socket = io();
 
    
    function gameOver() {
        $hangmanPage.fadeOut();
        $endPage.show();
    }
    
    function drawPlaceholder() {        
        for (var i=0; i<word.length; i++) {
            $placeholder.prepend('<li>_</li>');
        }
    }
    
    function showResult(data, position) {
        var $placeholderLi = $('ul#placeholder li');        
        
        var array = $placeholderLi.slice();
        array[position] = '<li>' + data.letter + '</li>';
        $placeholderLi.remove();
        
        for (var i=array.length-1;i>=0;i--) {
            $placeholder.prepend(array[i]);
        }
    }
    
    function drawPicture() {
        var picture = document.querySelector('#pictureHangman');
        var draw = picture.getContext('2d');
        
        draw.beginPath();
        
        // draw board
        draw.moveTo(0,350);
        draw.lineTo(600,350);
        
        // first mistake
        if (mistake>0) {
            draw.moveTo(150,350);
            draw.lineTo(150,50);
        }
        
        // second mistake
        if (mistake>1) {
            draw.moveTo(150,50);
            draw.lineTo(350,50);
        }
        
        // third mistake
        if (mistake>2) {
            draw.moveTo(350,50);
            draw.lineTo(350,80);
        }
        
        // fourth mistake
        if (mistake>3) {
            draw.arc(350,100,20,-1.5,2*Math.PI);
        }
        
        // fifth mistake
        if (mistake>4) {
            draw.moveTo(350,120);
            draw.lineTo(350,200);
        }
        
        // sixth mistake
        if (mistake>5) {
            draw.moveTo(350,140);
            draw.lineTo(320,180);
            draw.moveTo(350,140);
            draw.lineTo(380,180);
        }
        
        // seventh mistake
        if (mistake>6) {
            draw.moveTo(350,200);
            draw.lineTo(320,260);
            draw.moveTo(350,200);
            draw.lineTo(380,260);
            gameOver();
        }
            
        draw.stroke();
       
    }
    
    function setUsername() {
        username = cleanInput($nicknameInput.val().trim());

        if (username) {
          $loginPage.fadeOut();
          $hangmanPage.show();
          $loginPage.off('click');

          socket.emit('add user', username);
        }
    }    
    
    function sendLetter() {
        letter = cleanInput($letterInput.val());
        socket.emit('new letter', letter);
        $letterInput.val('');
    }
    
    function correctLetter(data) {
        if (word.indexOf(data.letter) === -1){
            ++mistake;
            drawPicture();
        } else {
            var position = word.indexOf(data.letter);
            points += 10;
            addPoints({
                username: username,
                points: points
            })
            showResult(data, position);
            
            socket.emit('add points', points);
        }
    }
    
    function addPoints(data) {
        $usersList.append('<li>' + data.username + ' ' + data.points +'</li>');
        
        var $usersListLi = $('#usersList li');
        $usersListLi.remove();
        $usersList.append('<li>' + data.username + ' ' + data.points +'</li>');
    }
    
    function cleanInput (input) {
        return $('<div/>').text(input).text();
    }    
   
    function setUsersList (data) {
        $usersList.append('<li>' + data.username + '</li>');
    }
    
    $window.keydown(function (event) {
        if (event.which === 13){
            if(username) {
                sendLetter();
            } else {
                setUsername();  
                drawPlaceholder();
                drawPicture();
            }
        }
    });

    
    $loginPage.click(function() {
        $currentInput.focus();
    })
    
    socket.on('users list', function(data) {
        setUsersList(data);
    });
    
    socket.on('new letter', function(data) {
        correctLetter(data);
        $lettersList.append('<li>' + data.letter + '</li>');
    }); 
    
    socket.on('add points', function(data) {
        addPoints(data); 
    });
});