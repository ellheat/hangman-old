var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

http.listen(3001, function(){
  console.log('listening on *:3001');
});

app.use(express.static(__dirname + '/'));


var usernames = {};

io.on('connection', function(socket){
    var addedUser = false;
    
    socket.on('add user', function(username) {
        socket.username = username;
        
        usernames[username] = username;
        addedUser = true;
        
        io.emit('users list', {
            username: socket.username
        });
    });
    
    socket.on('new letter', function(letter) {
        socket.letter = letter;
        
        socket.emit('new letter', {
            letter: socket.letter
        });
   //     io.emit('new letter', data);
    });

    socket.on('add points', function(points) {
        
        socket.emit('add points', {
            username: socket.username,
            points: points
        });
    });
    
    socket.on('disconnect', function() {
        delete usernames[socket.username];
    });
    
});